
val parserCombinators = "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.5"

lazy val root = (project in file(".")).
  settings(
    name := "lab1",
    scalaVersion := "2.12.2",
    libraryDependencies += parserCombinators
  )
